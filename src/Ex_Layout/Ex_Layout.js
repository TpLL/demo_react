import React, { Component } from 'react';
import Header from './Header';
import Navigation from './Navigation';
import Content from './Content';
import Footer from './Footer';

export default class Ex_Layout extends Component {
    render() {
        return (
            <div className="container">
                <Header />
                <div className="row">
                    <div className="col-6 pr-0">
                        <Navigation />
                    </div>
                    <div className="col-6 pl-0">
                        <Content />
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

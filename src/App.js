import logo from './logo.svg';
import './App.css';
import DemoClass from './DemoComponent/DemoClass';
import DemoFunction from './DemoComponent/DemoFunction';
import DataBingding from './DataBinding/DataBingding';
import Ex_Layout from './Ex_Layout/Ex_Layout';

function App() {
    return (
        <div className="App">
            {/* <DemoClass />
            <DemoFunction /> */}
            {/* <DataBingding /> */}
            <Ex_Layout />
        </div>
    );
}
export default App;

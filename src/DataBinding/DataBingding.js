import React, { Component } from 'react';

export default class DataBingding extends Component {
    imgSrc = 'https://i.ytimg.com/vi/A_o2qfaTgKs/maxresdefault.jpg';
    render() {
        let title = 'Bún bò huế';

        return (
            <div className="text-danger">
                <div className="card" style={{ width: '18rem', color: 'blue' }}>
                    <img src={this.imgSrc} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{title}</h5>
                        <p className="card-text">
                            Some quick example text to build on the card title
                            and make up the bulk of the card's content.
                        </p>
                        <a href="#" className="btn btn-primary">
                            Go somewhere
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}
